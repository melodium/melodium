use root/file::read as fullRead
use root/file::write as fullWrite
use root/filesystem::FileSystem
use root/local::|local_filesystem
use std/flow::emit
use std/flow::trigger
use std/ops/option/block::unwrap
use std/text/convert/string::toUtf8

/**
Write one file.

The bytes received through `data` are written in the file located at `path`.
The writing behavior is set up by the parameters:
- `append`: bytes are added to the file instead of replacing the existing file;
- `create`: if the file does not exists, it is created;
- `new`: the file is required to being new, if a file already exists at that path then the writing fails.

The amount of written bytes is sent through `amount`. There is no guarantee about its increment, as an undefined number of bytes may be written at once.
`finished` is emitted when successful writting is finished. `failure` is emitted if an error occurs, and `error` contains the related text of error(s).
*/
treatment write(path: string, append: bool = false, create: bool = true, new: bool = false)
  input data: Stream<byte>
  output finished: Block<void>
  output failure: Block<void>
  output error: Stream<string>
  output amount: Stream<u128>
{
    triggerData: trigger<byte>()
    emitFilename: emit<string>(value=path)
    emitFileSystem: emit<Option<FileSystem>>(value=|local_filesystem())
    unwrapFileSystem: unwrap<FileSystem>()

    fullWrite(append=append, create=create, new=new)

    Self.data -> triggerData.stream,start ---> emitFilename.trigger,emit ----------------------------------> fullWrite.path
                 triggerData.start --------> emitFileSystem.trigger,emit -> unwrapFileSystem.option,value -> fullWrite.filesystem
    Self.data ---------------------------------------------------------------------------------------------> fullWrite.data

    fullWrite.finished -> Self.finished
    fullWrite.failure --> Self.failure
    fullWrite.error ----> Self.error
    fullWrite.amount ---> Self.amount
}

/**
Write text in one file.

The text received through `text` is written as UTF-8 in the file located at `path`.
The writing behavior is set up by the parameters:
- `append`: bytes are added to the file instead of replacing the existing file;
- `create`: if the file does not exists, it is created;
- `new`: the file is required to being new, if a file already exists at that path then the writing fails.

The amount of written bytes is sent through `amount`. There is no guarantee about its increment, as an undefined number of bytes may be written at once.
`finished` is emitted when successful writting is finished. `failure` is emitted if an error occurs, and `error` contains the related text of error(s).
*/
treatment writeText(path: string, append: bool = false, create: bool = true, new: bool = false)
  input text: Stream<string>
  output finished: Block<void>
  output failure: Block<void>
  output error: Stream<string>
  output amount: Stream<u128>
{
    write(path=path, append=append, create=create, new=new)
    toUtf8()

    Self.text -> toUtf8.text,encoded -> write.data

    write.finished -> Self.finished
    write.failure --> Self.failure
    write.error ----> Self.error
    write.amount ---> Self.amount
}

treatment read(path: string)
  input  trigger: Block<void>
  output data: Stream<byte>
  output finished: Block<void>
  output failure: Block<void>
  output error: Stream<string>
  output reached: Block<void>
{
    emitFilename: emit<string>(value=path)
    emitFileSystem: emit<Option<FileSystem>>(value=|local_filesystem())
    unwrapFileSystem: unwrap<FileSystem>()

    fullRead()

    Self.trigger -> emitFilename.trigger,emit ------------------------------------> fullRead.path
    Self.trigger -> emitFileSystem.trigger,emit -> unwrapFileSystem.option,value -> fullRead.filesystem

    fullRead.data ------> Self.data
    fullRead.finished --> Self.finished
    fullRead.failure ---> Self.failure
    fullRead.error -----> Self.error
    fullRead.reached ---> Self.reached
}